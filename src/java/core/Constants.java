package core;

public class Constants
{
    public static final int BOARD_PANEL_WIDTH = 1512;
    public static final int BOARD_PANEL_HEIGHT = 1041;
    public static final int USERS_PANEL_WIDTH = 392;
    public static final int USERS_PANEL_HEIGHT = 761;
    public static final int CARD_PANEL_WIDTH = 1512;
    public static final int CARD_PANEL_HEIGHT = 280;
    public static final int BUTTONS_PANEL_WIDTH = 392;
    public static final int BUTTONS_PANEL_HEIGHT = 280;

    public static final int CAMERA_SPEED = 5;
    public static final int SCALE_INCREMENT = 100;
    public static final int MAX_SCALE = 1000;
    public static final int OUT_OF_BOUNDS = 1000;
}
