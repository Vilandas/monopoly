package core;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import static core.Constants.BOARD_PANEL_HEIGHT;

/**
 *
 * @author Vilandas Morrissey
 */
public class Enums
{
    public enum JailStatus
    {
        NOT_IN_JAIL,
        IN_JAIL,
        JAIL_1,
        JAIL_2,
        JAIL_3
    }

    public enum PlayerCommands
    {
        NAME,
        CHAT,
        ROLL_DICE,
        DISPLAY_CURRENT_PROPERTY,
        MANAGE_PROPERTIES,
        TRADE,
        PAY_FEE,
        END_TURN;

        public String getName()
        {
            return name();
        }

        @Override
        public String toString()
        {
            String name = name();
            name = name.replaceAll("_", " ");
            return name.charAt(0) + name.substring(1).toLowerCase();
        }
    }

    public enum Directions
    {
        UP,
        DOWN,
        LEFT,
        RIGHT;
    }

    public enum Images
    {
        BOARD("src/images/base/board.png", BOARD_PANEL_HEIGHT, BOARD_PANEL_HEIGHT);

        private String source;
        private int width;
        private int height;

        Images(String source, int width, int height)
        {
            this.source = source;
            this.width = width;
            this.height = height;
        }

        public Image getImg()
        {
            try
            {
                return ImageIO.read(new File(source));
            }
            catch (IOException e)
            {
                System.err.println("Could not load image: " + source);
                e.printStackTrace();
            }
            return null;
        }

        public int getWidth()
        {
            return width;
        }

        public int getHeight()
        {
            return height;
        }
    }
}