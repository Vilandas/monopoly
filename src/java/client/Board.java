package client;

import core.Enums;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

import static core.Constants.BOARD_PANEL_HEIGHT;
import static core.Constants.BOARD_PANEL_WIDTH;
import static core.Constants.CAMERA_SPEED;
import static core.Constants.MAX_SCALE;
import static core.Constants.OUT_OF_BOUNDS;
import static core.Constants.SCALE_INCREMENT;

public class Board extends JPanel
{
    private HashMap<Enums.Directions, Boolean> directions;
    private HashMap<Enums.Images, Image> images;
    private int xPos;
    private int yPos;
    private int velX;
    private int velY;
    private int scale;
    private boolean zoomed;

    public Board()
    {
        this.images = new HashMap();
        images.put(Enums.Images.BOARD, Enums.Images.BOARD.getImg().getScaledInstance(Enums.Images.BOARD.getWidth(), Enums.Images.BOARD.getHeight(), Image.SCALE_SMOOTH));

        this.directions = new HashMap();
        for (Enums.Directions direction : Enums.Directions.values())
        {
            this.directions.put(direction, false);
        }

        this.xPos = 0;
        this.yPos = 0;
        this.scale = 0;
        this.velX = 0;
        this.velY = 0;
        this.zoomed = false;
    }

    public void tick()
    {
        panCamera();
    }

    private void panCamera()
    {
        if(xPos + velX > -OUT_OF_BOUNDS && xPos + velX < BOARD_PANEL_WIDTH - OUT_OF_BOUNDS) xPos += velX;
        if(yPos + velY > -OUT_OF_BOUNDS && yPos + velY < BOARD_PANEL_HEIGHT - OUT_OF_BOUNDS) yPos += velY;

        if(directions.get(Enums.Directions.UP)) velY = -CAMERA_SPEED - (scale/SCALE_INCREMENT);
        else if(!directions.get(Enums.Directions.DOWN)) velY = 0;

        if(directions.get(Enums.Directions.DOWN)) velY = CAMERA_SPEED + (scale/SCALE_INCREMENT);
        else if(!directions.get(Enums.Directions.UP)) velY = 0;

        if(directions.get(Enums.Directions.LEFT)) velX = -CAMERA_SPEED - (scale/SCALE_INCREMENT);
        else if(!directions.get(Enums.Directions.RIGHT)) velX = 0;

        if(directions.get(Enums.Directions.RIGHT)) velX = CAMERA_SPEED + (scale/SCALE_INCREMENT);
        else if(!directions.get(Enums.Directions.LEFT)) velX = 0;
    }

    public void zoom(int zoom)
    {
        if(zoom > 0 && scale > -SCALE_INCREMENT)
        {
            scale -= SCALE_INCREMENT;
            zoomed = true;
            System.out.println(scale);
        }
        else if(zoom < 0 && scale < MAX_SCALE)
        {
            scale += SCALE_INCREMENT;
            zoomed = true;
        }
    }

    public HashMap<Enums.Directions, Boolean> getDirections()
    {
        return this.directions;
    }

    public void setDirection(Enums.Directions direction, boolean value)
    {
        directions.put(direction, value);
    }

    @Override
    public void paint(Graphics g)
    {

        g.setColor(Color.BLACK);
        g.fillRect(0, 0, BOARD_PANEL_WIDTH, BOARD_PANEL_HEIGHT);

        if(zoomed)
        {
            for(Enums.Images key : images.keySet())
            {
                images.put(key, key.getImg().getScaledInstance(key.getWidth() + scale, key.getHeight() + scale, Image.SCALE_SMOOTH));
            }
            zoomed = false;
        }

        for(Image image : images.values())
        {
            g.drawImage(image, xPos, yPos, null);
        }

        g.dispose();
    }
}
