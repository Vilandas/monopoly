package client;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Vilandas Morrissey
 */
public class Window extends JFrame
{
    private Client game;
    private JPanel panelStart;
    public Window(int width, int height, String title, Client client)
    {
        super(title);
        this.game = client;

        //Set size of frame
        this.setPreferredSize(new Dimension(width, height));
        this.setMinimumSize(new Dimension(width, height));
        this.setMaximumSize(new Dimension(width, height));

        this.game.setSize(new Dimension(width, height));
        this.game.setMinimumSize(new Dimension(width, height));
        this.game.setMaximumSize(new Dimension(width, height));

        //this.setBackground(Color.black);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Start the game in the center of the window
        this.setLocationRelativeTo(null);

        //Add class to window
        this.add(this.game);

        createStartMenu();
        //createGameArea();

        this.setVisible(true);
    }

    private void createStartMenu()
    {
        panelStart = new JPanel();
        panelStart.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        JTextField tFieldName = new JTextField("Daniel", 12);
        JTextField tFieldIP = new JTextField("localhost", 12);
        JTextField tFieldPort = new JTextField("50000", 12);

        JLabel labelName = new JLabel("Your name: ");
        JLabel labelIP = new JLabel("IP Address: ");
        JLabel labelPort = new JLabel("Port: ");
        labelName.setLabelFor(tFieldName);
        labelIP.setLabelFor(tFieldIP);
        labelPort.setLabelFor(tFieldPort);

        JPanel panelStartLabels = new JPanel();
        JPanel panelStartFields = new JPanel();
        panelStartLabels.setLayout(new GridLayout(0, 1, 0, 10));
        panelStartFields.setLayout(new GridLayout(0, 1, 0, 10));

        panelStartLabels.add(labelName);
        panelStartLabels.add(labelIP);
        panelStartLabels.add(labelPort);
        panelStartFields.add(tFieldName);
        panelStartFields.add(tFieldIP);
        panelStartFields.add(tFieldPort);

        JButton buttonConnect = new JButton("Connect To Server");
        buttonConnect.addActionListener(e ->
        {
            //TODO proper error handling
            buttonConnect.setEnabled(false);
            game.chooseName(tFieldName.getText());
            if(game.connectToServer(tFieldIP.getText(), Integer.parseInt(tFieldPort.getText())))
            {
                panelStart.setVisible(false);
                game.createGameArea();
                game.start();
            }
            else buttonConnect.setEnabled(true);
        });

        panelStart.add(panelStartLabels, BorderLayout.CENTER);
        panelStart.add(panelStartFields, BorderLayout.LINE_END);
        panelStart.add(buttonConnect);

        this.add(panelStart, BorderLayout.CENTER);
    }
}
