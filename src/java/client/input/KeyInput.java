package client.input;

import client.Board;
import core.Enums;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyInput extends KeyAdapter
{
    Board board;

    public KeyInput(Board board)
    {
        this.board = board;
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_UP) board.setDirection(Enums.Directions.UP, true);
        if(key == KeyEvent.VK_LEFT) board.setDirection(Enums.Directions.LEFT, true);
        if(key == KeyEvent.VK_DOWN) board.setDirection(Enums.Directions.DOWN, true);
        if(key == KeyEvent.VK_RIGHT) board.setDirection(Enums.Directions.RIGHT, true);
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_UP) board.setDirection(Enums.Directions.UP, false);
        if(key == KeyEvent.VK_LEFT) board.setDirection(Enums.Directions.LEFT, false);
        if(key == KeyEvent.VK_DOWN) board.setDirection(Enums.Directions.DOWN, false);
        if(key == KeyEvent.VK_RIGHT) board.setDirection(Enums.Directions.RIGHT, false);
    }
}
