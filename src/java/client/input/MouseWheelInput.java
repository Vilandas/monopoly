package client.input;

import client.Board;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public class MouseWheelInput implements MouseWheelListener
{
    private Board board;

    public MouseWheelInput(Board board)
    {
        this.board = board;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e)
    {
        board.zoom(e.getWheelRotation());
    }
}
