package client;

import client.input.KeyInput;
import client.input.MouseWheelInput;
import core.Colours;
import core.Constants;
import core.Enums;
import server.Server;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import static core.Constants.BOARD_PANEL_HEIGHT;
import static core.Constants.BOARD_PANEL_WIDTH;

/**
 *
 * @author Vilandas Morrissey
 */
public class Client extends JPanel implements Runnable
{
    private Socket dataSocket;
    private Scanner keyboard;
    private Server server;
    private String inKey;
    private String name;

    private boolean isRunning;
    private Thread thread;
    private boolean activePlayer;

    private OutputStream out;
    private PrintWriter output;
    private InputStream in;
    private Scanner input;

    private Board board;
    private JPanel panelCards;
    private JPanel panelButtons;
    private JPanel panelUsers;

    public static void main(String args[])
    {
        new Client();
    }

    public Client()
    {
        this.inKey = Colours.BLUE + "\n)> " + Colours.RESET;
        this.keyboard = new Scanner(System.in);
        this.dataSocket = null;
        this.activePlayer = false;

        this.setBackground(Color.black);
        this.setBorder(BorderFactory.createLineBorder(Color.RED));
        this.setVisible(false);
        new Window(1920, 1080, "Monopoly", this);
    }

    public void chooseName(String name)
    {
        this.name = name;
    }

    public void start()
    {
        isRunning = true;
        thread = new Thread(this);
        thread.start();
    }

    private void stop()
    {
        isRunning = false;
        try
        {
            thread.join();
        } catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void run()
    {
        this.requestFocus();
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int frames = 0;
        while(isRunning)
        {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while(delta >= 1)
            {
                tick();
                //updates++;
                delta--;
            }
            render();
            frames++;

            if(System.currentTimeMillis() - timer > 1000)
            {
                timer += 1000;
                frames = 0;
                //updates = 0;
            }
        }
        stop();
    }

    public void tick()
    {
        board.tick();
    }

    public void render()
    {
        board.paintImmediately(0, 0, BOARD_PANEL_WIDTH, BOARD_PANEL_HEIGHT);
    }

    /**
     * Take input from user and covert it to an integer.
     * @param positiveOnly if true, will not return numbers below 0
     * @return integer number
     */
    private int getInt(boolean positiveOnly)
    {
        while(true)
        {
            String input = keyboard.nextLine();
            try
            {
                int parsedInt = Integer.parseInt(input);
                if(!positiveOnly)
                {
                    return parsedInt;
                }
                else if(parsedInt >= 0)
                {
                    return parsedInt;
                }
                System.err.println("The number must not be a negative number");
            } catch (NumberFormatException e)
            {
                System.err.println("This is not a valid integer number. Try again");;
            }
        }
    }

    /**
     * Take input from user and check to see if it is equal to or greater/less than the set lower and upper values
     * @param lower equal to or greater than this number
     * @param upper equal to or less than this number
     * @return number between and including upper and lower
     */
    private int getIntRange(int lower, int upper)
    {
        while(true)
        {
            int parsedInt = getInt(false);
            if(parsedInt >= lower && parsedInt <= upper)
            {
                return parsedInt;
            }
            System.err.println("This number is not in the range of " + lower + "-" + upper);
        }
    }

    /**
     * Check for commands within the response and act upon certain commands.
     * @param line The line to check for valid command
     */
    private void checkLineContainsCommand(String line)
    {
        if (line.equals(Enums.PlayerCommands.END_TURN))
        {
            this.activePlayer = false;
        }
    }

    /**
     * Send Command to the server and wait for response.
     * @param command To send to the server.
     */
    public void sendCommandAndReceive(Enums.PlayerCommands command)
    {
        try
        {
            if (this.dataSocket != null)
            {
                output.println(command.getName());
                output.println(name);
                if(command.getName().equals(Enums.PlayerCommands.NAME))
                {
                    output.println(name);
                }

                String response = "";
                //Read until final "}" closing bracket
                while (!response.equals("}"))
                {
                    response = this.input.nextLine();
                    checkLineContainsCommand(response);
                }
            } else System.out.println("No connection to server");
        }
        catch(NoSuchElementException e)
        {
            //Set dataSocket to null and restart while loop in Run() method
            System.out.println("Disconnected from server");
            this.dataSocket = null;
        }
    }

    /**
     * Build the input/output streams for sending and receiving from the server.
     * @throws IOException
     */
    private void buildIOStreams() throws IOException
    {
        out = this.dataSocket.getOutputStream();
        output = new PrintWriter(new OutputStreamWriter(out), true);
        in = this.dataSocket.getInputStream();
        input = new Scanner(new InputStreamReader(in));
    }

    /**
     * Continuously attempts to connect to the server. Upon connection, will try to update valid vehicles list.
     * Afterwards will continuously check for TollEvents in the tolls list and process them to the server.
     */
    public boolean connectToServer(String ip, int port)
    {
        try
        {
            //Establish a connection with server
            dataSocket = new Socket(ip, port);
            System.out.println("Connection to server established");

            buildIOStreams();
            output.println(name);
            return true;
        }
        catch(ConnectException e)
        {
            System.out.println("Could not establish a connection to server");
        }
        catch(UnknownHostException e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        catch(NoSuchElementException e)
        {
            e.printStackTrace();
            System.out.println("Error, server closed: " + e.getMessage());
        }
        finally
        {
            try
            {
                if(this.dataSocket != null)
                {
                    this.dataSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println(e.getMessage());
                System.exit(1);
            }
        }
        return false;
    }

    public void createGameArea()
    {
        board = new Board();
        board.setPreferredSize(new Dimension(Constants.BOARD_PANEL_WIDTH, Constants.BOARD_PANEL_HEIGHT));
        board.setBackground(Color.GRAY);
        board.setBounds(392, 0, Constants.BOARD_PANEL_WIDTH, Constants.BOARD_PANEL_HEIGHT);

        panelUsers = new JPanel();
        panelUsers.setPreferredSize(new Dimension(Constants.USERS_PANEL_WIDTH, Constants.USERS_PANEL_HEIGHT));
        panelUsers.setBackground(Color.BLUE);
        panelUsers.setBounds(0, 0, Constants.USERS_PANEL_WIDTH, Constants.USERS_PANEL_HEIGHT);

        panelCards = new JPanel();
        panelCards.setPreferredSize(new Dimension(Constants.CARD_PANEL_WIDTH, Constants.CARD_PANEL_HEIGHT));
        panelCards.setBackground(new Color(255,0,0,70));
        panelCards.setBounds(392, 761, Constants.CARD_PANEL_WIDTH, Constants.CARD_PANEL_HEIGHT);

        panelButtons = new JPanel();
        panelButtons.setPreferredSize(new Dimension(Constants.BUTTONS_PANEL_WIDTH, Constants.BUTTONS_PANEL_HEIGHT));
        panelButtons.setBackground(Color.GREEN);
        panelButtons.setBounds(0, 761, Constants.BUTTONS_PANEL_WIDTH, Constants.BUTTONS_PANEL_HEIGHT);

        this.setLayout(null);
        this.add(panelUsers);
        this.add(panelCards);
        this.add(panelButtons);
        this.add(board);

        this.addKeyListener(new KeyInput(this.board));
        this.addMouseWheelListener(new MouseWheelInput(this.board));
        this.setFocusable(true);
        this.requestFocus();
        this.setVisible(true);
    }
}
