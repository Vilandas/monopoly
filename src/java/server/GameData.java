package server;

import java.net.Socket;

/**
 *
 * @author Vilandas Morrissey
 */
public class GameData
{
    private Player[] players;
    private int playerIndex;

    public GameData(int playerCount)
    {
        this.players = new Player[playerCount];
        this.playerIndex = 0;
    }

    /**
     * Add player to game
     * @param group
     * @param threadName
     * @param dataSocket
     * @param money
     */
    public void addPlayer(ThreadGroup group, String threadName, Socket dataSocket, int money)
    {
        players[playerIndex] = new Player(group, threadName, dataSocket, money);
        playerIndex++;
    }

    public int getPlayerCount()
    {
        int count = 0;
        for (int i = 0; i < players.length; i++)
        {
            if(players[i] != null)
                count++;
        }
        return count;
    }
}
