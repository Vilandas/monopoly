package server;

import core.Enums;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class Player extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;

    private ArrayList<Integer> ownedProperties;
    private String playerName;
    private int money;
    private int position;
    private int jailFreeCards;
    private int doubleRollCount;
    private Enums.JailStatus jailStatus;
    private boolean rolledDice;
    private boolean isPlaying;

    /**
     *
     * @author Vilandas Morrissey
     */
    public Player(ThreadGroup group, String threadName, Socket dataSocket, int money)
    {
        super(group, threadName);
        try
        {
            this.dataSocket = dataSocket;
            this.input = new Scanner(new InputStreamReader(this.dataSocket.getInputStream()));
            this.output = new PrintWriter(this.dataSocket.getOutputStream(), true);
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }

        this.playerName = input.nextLine();
        this.money = money;
        this.ownedProperties = new ArrayList();
        this.position = 0;
        this.jailFreeCards = 0;
        this.doubleRollCount = 0;
        this.jailStatus = Enums.JailStatus.NOT_IN_JAIL;
        this.rolledDice = false;
        this.isPlaying = true;
        System.out.println(playerName);
    }

    public void displayOwnedProperties()
    {

    }

    public boolean manageTradeOffer(int playerIndex, int price, Property property, boolean jailFreeCard)
    {
        return false;
    }

    public void move()
    {
//        int dice1 = random.nextInt(6) + 1;
//        int dice2 = random.nextInt(6) + 1;
//        if(dice1 == dice2)
//        {
//
//        }
    }

    public ArrayList<Integer> getOwnedProperties()
    {
        return ownedProperties;
    }

    public String getPlayerName()
    {
        return playerName;
    }

    public int getMoney()
    {
        return money;
    }

    public int getPosition()
    {
        return position;
    }

    public int getJailFreeCards()
    {
        return jailFreeCards;
    }

    public int getDoubleRollCount()
    {
        return doubleRollCount;
    }

    public Enums.JailStatus getJailStatus()
    {
        return jailStatus;
    }

    public boolean hasRolledDice()
    {
        return rolledDice;
    }

    public boolean isPlaying()
    {
        return isPlaying;
    }
}
