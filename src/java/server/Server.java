package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import static core.Details.*;

/**
 *
 * @author Vilandas Morrissey
 */
public class Server
{
    private GameData gameData;
    private int startingMoney;
    private int maxPlayerCount;

    private Thread connectionListener;

    //TODO Ask for input on number of players and starting money
    public static void main(String args[])
    {
        Server server = new Server(2, 1500);
    }


    public Server(int maxPlayerCount, int startingMoney)
    {
        this.maxPlayerCount = maxPlayerCount;
        this.startingMoney = startingMoney;
        this.gameData = new GameData(maxPlayerCount);
        this.connectionListener = new Thread(() -> listenForConnections());
        this.connectionListener.start();
    }

    /**
     * Continuously listen for connections and create a new player for each connection
     */
    private void listenForConnections()
    {
        ServerSocket listeningSocket = null;
        try
        {
            //Set up a connection socket to listen for connections
            listeningSocket = new ServerSocket(LISTENING_PORT);

            //Set up a ThreadGroup to manage all of the clients threads
            ThreadGroup clientThreadGroup = new ThreadGroup("Client Threads");
            clientThreadGroup.setMaxPriority(Thread.currentThread().getPriority() - 1);

            boolean continueRunning = true;
            while(continueRunning)
            {
                //Wait for an incoming connection
                Socket dataSocket = listeningSocket.accept();
                System.out.println("Connection from: " + dataSocket.getInetAddress());
                if(gameData.getPlayerCount() < maxPlayerCount)
                {
                    gameData.addPlayer(clientThreadGroup, dataSocket.getInetAddress().toString(), dataSocket, startingMoney);
                }
                //TODO Check for player disconnects and allow to reconnect
                else
                {
                    System.out.println("Connection refused: Server is full");
                }
            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
